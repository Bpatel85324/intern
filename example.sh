            abcd=$(mkdir -p ~/bin && curl -sSL -o ~/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64 && chmod +x ~/bin/jq)
            export PATH=$PATH:~/bin
            jq --version
            LIST_TAG=$(git tag)
            LAST_RELEASE=$(git describe --tags --match v* | perl -ne '{ /(v[0-9\.]+)/ && print "$1\n" }')
            CHANGE_LIST=$(git log $LAST_RELEASE..$CI_COMMIT_SHA --pretty=oneline | perl -ne '$sq = "\047"; { /\w+: (\w+)-(\d+)/ && print "$sq$1-$2$sq\n" }' | sort | uniq | paste -sd "\n")
            echo "issue in ($CHANGE_LIST) and status = 'AWAITING RELEASE'"
            for i in $(echo $CHANGE_LIST| tr "'" "\n")
            do
            echo $i
            for j in $(echo $i | sed -e $'s/-/\\\n/g')
            do
                CURR_ID=$(echo $j)
                break
            done
            echo $CURR_ID
            #DIFF_PRO=$(echo $i| tr - '\n')
            #echo $DIFF_PRO
            #FETCH_PRO=$(echo $DIFF_PRO| tr " " "\n")
            #echo $FETCH_PRO
            PROJECT_ID=$(curl -X GET \
                          'https://1800flowersinc.atlassian.net/rest/api/2/project/'$CURR_ID'?expand=' \
                          -H 'authorization: Basic YnBhdGVsQDE4MDBmbG93ZXJzLmNvbTpJVUJxczdBY3JNVElNYzlQU1lSN0FEQTQ=' \
                          -H 'cache-control: no-cache,no-cache' \
                          -H 'content-type: application/json' \
                          -H 'postman-token: b3310b79-1adc-5bca-973d-1664d8a3f35e' \
                          -d '{
                            "transition":
                            {
                                "id": "31" 
                            }
                        }')
                                
                                
            echo $PROJECT_ID > a.json
            SHOW_PRO_ID=$(cat a.json| jq '.id')
            echo $SHOW_PRO_ID
            rm -f a.json
            SET_VERSION=$(curl --request POST \
                     --url 'https://1800flowersinc.atlassian.net/rest/api/3/version' \
                     --header 'Authorization: Basic YnBhdGVsQDE4MDBmbG93ZXJzLmNvbTpJVUJxczdBY3JNVElNYzlQU1lSN0FEQTQ' \
                     --header 'Accept: application/json' \
                     --header 'Content-Type: application/json' \
                     --data '{
                    "archived": false,
                     "name": "'$LIST_TAG'",
                    "projectId": '$SHOW_PRO_ID',
                    "released": false
                     }')
            echo "[$LIST_TAG]"
            curl -X PUT 'https://1800flowersinc.atlassian.net/rest/api/2/issue/'$i -H 'authorization: Basic YnBhdGVsQDE4MDBmbG93ZXJzLmNvbTpJVUJxczdBY3JNVElNYzlQU1lSN0FEQTQ=' \
                            -H 'cache-control: no-cache' \
                            -H 'content-type: application/json' \
                            -H 'postman-token: b3310b79-1adc-5bca-973d-1664d8a3f35e' \
                            -d '{ "update":{"fixVersions":  [  {"add":  {"name":"'$LIST_TAG'"} } ]  } }'
            sleep 3;
            done