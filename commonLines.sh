            abcd=$(mkdir -p ~/bin && curl -sSL -o ~/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64 && chmod +x ~/bin/jq)
            export PATH=$PATH:~/bin
            jq --version
            echo "THIS IS :$STATE_NAME"
            LAST_RELEASE=$(git describe --tags --match v* | perl -ne '{ /(v[0-9\.]+)/ && print "$1\n" }')
            CHANGE_LIST=$(git log $LAST_RELEASE..$CI_COMMIT_SHA --pretty=oneline | perl -ne '$sq = "\047"; { /\w+: (\w+)-(\d+)/ && print "$sq$1-$2$sq\n" }' | sort | uniq | paste -sd "\n")
            echo "issue in ($CHANGE_LIST) and status = 'AWAITING RELEASE'"
            chmod 755 example.sh
            for i in $(echo $CHANGE_LIST| tr "'" "\n")
                do
                        #echo "issues are $i"
                        FETCH_STATE=$(curl -X GET \
                            'https://1800flowersinc.atlassian.net/rest/api/2/issue/'$i'?fields=status' \
                            -H 'authorization: Basic YnBhdGVsQDE4MDBmbG93ZXJzLmNvbTpJVUJxczdBY3JNVElNYzlQU1lSN0FEQTQ=' \
                            -H 'cache-control: no-cache,no-cache' \
                            -H 'content-type: application/json' \
                            -H 'postman-token: b3310b79-1adc-5bca-973d-1664d8a3f35e' \
                            -d '{
                                    "transition":
                                    {
                                        "id": "31"
                                    }
                                    }')
                        echo $FETCH_STATE > a.json
                        SHOW_NAME=$(cat a.json| jq '.fields.status.name')
                        echo $SHOW_NAME
                        FETCH_ID=$(curl -X GET \
                              'https://1800flowersinc.atlassian.net/rest/api/2/issue/'$i'/transitions' \
                              -H 'authorization: Basic YnBhdGVsQDE4MDBmbG93ZXJzLmNvbTpJVUJxczdBY3JNVElNYzlQU1lSN0FEQTQ=' \
                              -H 'cache-control: no-cache,no-cache' \
                              -H 'content-type: application/json' \
                              -H 'postman-token: b3310b79-1adc-5bca-973d-1664d8a3f35e' \
                              -d '{
                                "transition":
                                {
                                    "id": "31" 
                                }
                                }')
                        echo $FETCH_ID > b.json
                        echo $FETCH_ID > c.json
                        SHOW_ID=$(cat b.json| jq '.transitions[].to.name')
                        echo $SHOW_ID
                        
                        SHOW_IDS=$(cat c.json| jq '.transitions[].id')
                        echo $SHOW_IDS
                        INC_ONE=$(echo "0")
                        COMP_VAR=$(echo $SHOW_ID| tr " " '-')
                        
                        if [ "$SHOW_NAME" == '"Open"' ]; then
                            exit 1;
                        fi
                        if [ "$STATE_NAME" == '"In Progress"' ]; then
                            MATCH_IT=$(echo "Review")
                        fi
                        
                        if [ "$STATE_NAME" == '"Review"' ]; then
                            MATCH_IT=$(echo "Test")
                        fi
                        
                        if [ "$STATE_NAME" == '"Test"' ]; then
                            MATCH_IT=$(echo "Stage")
                        fi
                        
                        if [ "$STATE_NAME" == '"Stage"' ]; then
                            MATCH_IT=$(echo "Done")
                        fi
                        
                        for j in $(echo $COMP_VAR| tr '"' '\n')
                        do
                            #echo $j
                            INC_ONE=$((INC_ONE + 1))
                            if [ "$j" == "$MATCH_IT" ]; then
                                #echo "match1"
                                break
                            fi
                            
                            
                            
                            #echo $INC_ONE
                            
                        done
                        INC_SEC=$(echo "0")
                        COMP_ID=$(echo $SHOW_IDS| tr " " '-')
                        for k in $(echo $COMP_ID| tr '"' '\n')
                        do
                            #echo $k
                            INC_SEC=$((INC_SEC + 1))
                            if [ "$INC_SEC" == "$INC_ONE" ]; then
                                #echo "match2"
                                THIS_ONE=$(echo $k)
                                break
                            fi
                        done
                        echo $THIS_ONE
                        
                        
                        if [ "$SHOW_NAME" == "$STATE_NAME" ]; then
                            #echo "x has the value 'valid'"
                            
                            curl -X POST \
                                'https://1800flowersinc.atlassian.net/rest/api/3/issue/'$i'/transitions?expand=transitions.fields' \
                                -H 'authorization: Basic YnBhdGVsQDE4MDBmbG93ZXJzLmNvbTpJVUJxczdBY3JNVElNYzlQU1lSN0FEQTQ=' \
                                -H 'cache-control: no-cache' \
                                -H 'content-type: application/json' \
                                -H 'postman-token: b3310b79-1adc-5bca-973d-1664d8a3f35e' \
                                -d '{
                                     "transition":
                                    {
                                    "id": "'$THIS_ONE'"
                                    }
                                }';
                        fi
                        rm -f a.json
                        rm -f b.json
                        rm -f c.json
                        sleep 3;
                done
                